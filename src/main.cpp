#include <Arduino.h>
#include "secrets.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Roomba.h>
#include <ArduinoOTA.h>

////////////////////////////////////////////////////////////////////////
// FOR DEBUGGING DEV only
WiFiServer TelnetServer(23);
WiFiClient Telnet;
//////////////////////////////////////////////////////////////////////////////

// Roomba setup
Roomba roomba(&Serial, Roomba::Baud115200);

// Network setup
WiFiClient espClient;

// MQTT setup
PubSubClient client(espClient);
const PROGMEM char *commandTopic = MQTT_COMMAND_TOPIC;
const PROGMEM char *statusTopic = MQTT_STATE_TOPIC;


/**
 * FOR DEBUGGING ONLY
 */
void handleTelnet(){
  if (TelnetServer.hasClient()) {
    //client is Connected
    if (!Telnet || !Telnet.connected()) {
      if (Telnet) Telnet.stop();    //Cliente desconectado
      Telnet = TelnetServer.available(); //ready for new client
    } else {
      TelnetServer.available().stop();  // have client
    }
  }
}
 void TelnetTime(){
   Telnet.println("uptime: " + (String)millis() + " ms");
   delay (2000);
 }

/////////
// END //
/////////



void callback(char* topic, byte* payload, unsigned int length)
{
  Telnet.print("Command from MQTT broker is : [");
  Telnet.print(topic);
  Telnet.print("] ");
  int p =(char)payload[0]-'0';
  if(p==0)
  {
    Telnet.println(" Me pongo a limpiar " );
    client.publish("vacuum/state", "Limpiando");
    roomba.start();
    delay(50);
    roomba.safeMode();
    delay(50);
    roomba.cover();
  }
  if(p==1)
  {
    Telnet.println(" Me voy pa mi casa " );
    client.publish("vacuum/state", "Retornando");
    roomba.start();
    delay(50);
    roomba.safeMode();
    delay(50);
    roomba.dock();
  }
  Telnet.println();
}

void setup() {
    // put your setup code here, to run once:
    // Set Hostname y conectar a wifi
    String hostname(HOSTNAME);
    WiFi.hostname(hostname);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while(WiFi.status() != WL_CONNECTED) {
      delay(100);
      Serial.print(".");
    }
    // 3 lineas para Arduino OTA 2 aquí y una en el loop
    ArduinoOTA.setHostname((const char *)hostname.c_str());
    ArduinoOTA.begin();
    // MQTT
    client.setServer(MQTT_SERVER, 1883);
    client.setCallback(callback);

    // Serial.begin(9600);
    Serial.begin(115200);



    ////////////// FOR DEBUGGING ONLY /////////////////////////////
    TelnetServer.begin();
    TelnetServer.setNoDelay(true);
    //////////////////////////////////////////////////////////////////////////////

}

void reconnect(){
  while (!client.connected()) {
    Telnet.println("\nConectando a servidor mqtt ");
    Telnet.println(MQTT_SERVER);
    if(client.connect("pues eso",MQTT_USER,MQTT_PASSWORD)){
      Telnet.println("\nConectado a servidor mqtt ");
      Telnet.println(MQTT_SERVER);
      client.subscribe(MQTT_COMMAND_TOPIC);
    } else {
      Telnet.println("\nReintentando conectar a servidor mqtt");
      delay(200);
    }
  }
}
void ConectadoMQTT(){
  if (client.connected()){
    Telnet.println("Cliente conectado");
  }
  if (!client.connected()) {
    Telnet.println("MQTT desconectado");
    Serial.println("MQTT desconectado");
  }
}
void loop() {
    // put your main code here, to run repeatedly:
    ArduinoOTA.handle();

    ////////////// FOR DEBUGGING ONLY /////////////////////////////
    handleTelnet();
    TelnetTime();
    //////////////////////////////////////////////////////////////////////////////
    ConectadoMQTT();
    if (!client.connected()){
      reconnect();
    }
  client.loop();
}
